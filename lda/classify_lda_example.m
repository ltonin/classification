% Example code to use LDA classifier
clc;clearvars;close all;

npat = 1000;

% create synthetic data
r1 = 0.5 + 0.95*randn(npat,5);
r2 = 1.5 + 0.95*randn(npat*2,5);
data = [r1;r2];
labels = [ones(1,size(r1,1)) 2*ones(1,size(r2,1))];

% train LDA
model = classify_train(data,labels,'lda');

% test LDA

[pp, res] = classify_eval(model,data);


accuracy  = 100*sum(res'==labels)/length(labels);

% graphics
figure(1)
plot(data(labels==1,1),data(labels==1,2),'.') ; hold on;
plot(data(labels==2,1),data(labels==2,2),'gx');
axis([0 6 0 6]);
mis_ind = find(res'~=labels);
plot(data(mis_ind,1),data(mis_ind,2),'ro','markersize',10);
legend('class-1', 'class-2', 'misclassified');
hold off

