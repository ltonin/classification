  % Fix stream of random numbers
  s1 = RandStream.create('mrg32k3a','Seed', 42);
  s0 = RandStream.setGlobalStream(s1);
  
  % Create data set
  n = 30; p = 400;
  correlation = 0.2;
  Sigma = correlation*ones(p) + (1 - correlation)*eye(p);
  mu = zeros(p,1);
  X = mvnrnd(mu, Sigma, n);
  % Model is lin.comb. of first three variables plus noise
  y = X(:,1) + X(:,2) + X(:,3) + 0.5*randn(n,1);
  % Preprocess data
  X = normalize(X);
  y = center(y);
  % Run LASSO
  delta = 1e3;
  [beta info] = elasticnet(X, y, delta, 0, true, true);
  % Plot results
  h1 = figure(1);
  plot(info.s, beta, '.-');
  xlabel('s'), ylabel('\beta', 'Rotation', 0)
  % Restore random stream
  RandStream.setGlobalStream(s0);
  
  
  
  Xtr = Cont_EMG_down( :, 1 : length(Cont_EMG_down)/2 )';
  Xts = Cont_EMG_down( :, length(Cont_EMG_down)/2+1 : end )';
  
  ytr = Cont_MOV_up( 10, 1 : length(Cont_MOV_up)/2 )';
  yts = Cont_MOV_up( 10, length(Cont_MOV_up)/2+1 : end )';
  
  Xtr = normalize(Xtr);
  Xts = normalize(Xts);
  ytr = center(ytr);
  yts = center(yts);
  
  delta = 1e-3;
  [beta info] = elasticnet(Xtr, ytr, delta, 0, true, true);

%   weights = beta( :, end );
%   
%   yptr = Xtr*weights; 
%   ypts = Xts*weights;
  
  for be = size(beta,2) : size(beta,2)
      
  weights = beta( :, be );
  
  yptr = Xtr*weights; 
  ypts = Xts*weights;

  
  fig = figure, 
  set(fig,'units','normalized','outerposition',[0 0 1 1]);

      subplot(2,1,1),
      plot(yptr)
      hold on, plot(ytr,'r')
      plot(smooth(yptr,50),'g')
%       axis([0 length(ytr) -100 200])
      legend({'Regression' 'Original' 'Regression Smooth 50' })
      ylabel('Train')
      
      subplot(2,1,2),
      plot(ypts)
      hold on, plot(yts,'r')
            plot(smooth(ypts,50),'g')

      
%       axis([0 length(ytr) -100 200])
      legend({'Regression' 'Original' 'Regression Smooth 50' })

            ylabel('Test')


      
  end
  
  
  