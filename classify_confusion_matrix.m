function [cm, cmperc, acc, err] = classify_confusion_matrix(label, guess)
% [cm, cmperc, acc, err] = classify_confusion_matrix(label, guess)
%
% Input :
%
%   label               True labels
%   guess               Predicted labels
%
% Output :
%   
%   cm                  Confusion matrix in percentage computed per class:
%                                 -------- ---------
%                                | Class1 | Class 2 |
%                        --------|--------|---------|
%                       | Class1 |   %    |    %    |
%                        --------|--------|---------|
%                       | Class2 |   %    |    %    |
%                        -------- -------- ---------
%
%   cmperc              Confusion matrix in percentage (overall)
%   acc                 Total accuracy in percentage
%   err                 Total error in percentage

    if isvector(label) == false || isvector(guess) == false
        error('[class] - Labels and guess must be vectors');
    end

    ntl = length(label);
    ngl = length(guess);
    
    if isequal(ntl, ngl) == false
        error('[class] - Labels and guess vectors must have the same size');
    end
    
    % Convert custom label to indexes [1 2 ...]
    label = grp2idx(label);
    guess = grp2idx(guess);
    
    nsamples = ntl;   
    classes  = unique(label);
    nclasses = length(classes);
    
    % Confusion matrix counting
    cmcount    = zeros(nclasses);
    for sId = 1:nsamples
        cmcount(label(sId), guess(sId)) = cmcount(label(sId), guess(sId)) + 1;
    end
    
    % Confusion matrix percentage
    cmperc = 100*cmcount/sum(cmcount(:));
    
    % Confusion matrix per class
    cm = zeros(nclasses);
    for cId = 1:nclasses
        cm(cId, :) = 100*cmcount(cId, :)/sum(cmcount(cId, :));
    end
    
    % Accuracy and error
    acc = trace(cmperc);
    err = 100 - acc;
end
