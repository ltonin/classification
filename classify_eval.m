function [pp, class] = classify_eval(model, F)
% Use:
%   [pp, class] = classify_eval(model, F)
% 
% Input :
%   model                   Struct containing the trained model,
%   F                       Feature vector [observations X features]
%
%  Output:
%   pp                      Posterior probabilities for the two classes
%   class                   Predicted labels
% 
% See also classify_lda_train, classify_qda_train, classify_knn_train, 
% classify_gau_train, classify_lda_eval, classify_qda_eval,
% classify_knn_eval, classify_gau_eval, classify_eval
    
    % switch to the chosen model
    switch model.type
        case 'lda'
            [pp, class] = classify_lda_eval(model, F);
        case 'qda'
            [pp, class] = classify_qda_eval(model, F);
        case 'knn'
            error('[classTest] - Not yet implemented');
            %[pp, class] = knn_test(model, F);
        case 'gau'
            error('[classTest] - Not yet implemented');
            %[pp, class] = gau_test(model, F);
        otherwise
            error('Error. Model type is not found'); 
    end
        
end