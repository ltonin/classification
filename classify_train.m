function model = classify_train(F, Fk, type, varargin)
% Use:
%   model = classify_train(F, Fk, type [, varargin])
% 
% Create and train a model for a 2 classes data.
%
% Input:
%   F                   Feature vector, [observations X features]
%   Fk                  Class labels, [observations X 1]. Only two
%                       classes allowed
%   type                Type of classifier:
%                        'lda'    --> Linear Discriminant Analysis
%                        'qda'    --> Quadratic Discriminant Analysis
%                        'knn'    --> k-nearest neighbor Classification
%                        'gau'    --> Gaussian Classifion
% Optional:
%                       Depending on the classifier
%                       (digit "help lda_train", "help qda_train"...)
% 
% Output:
%   model              = struct containing the trained model, fields vary
%                        with the type of chosen model
%
% See also classify_lda_train, classify_qda_train, classify_knn_train, classify_gau_train, classify_lda_test, classify_qda_test,
% classify_knn_test, classify_gau_test, classify_test

   
    % switch to the chosen model
    switch type
        case 'lda'
            model = classify_lda_train(F, Fk, varargin{:});
        case 'qda'
            model = classify_qda_train(F, Fk, varargin{:});
        case 'knn'
            error('[classTrain] - Not yet implemented');
            %model = knn_train(F, Fk, varargin);
        case 'gau'
            error('[classTrain] - Not yet implemented');
            %model = gau_train(F, Fk, varargin);
        otherwise
            error('Error. Classificator type is not found'); 
    end

end