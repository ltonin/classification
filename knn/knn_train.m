function [knnStruct] = knn_train(data,labels,options)
% Use:
%   [knnStruct] = qda_train(data,labels,options)
% 
% Input:
%   data                  = predictor data, N by d matrix (observations in
%                           rows, features in columns)
%   labels                = class labels, N by 1 vector (where value 1 
%                           stays for the first class and 2 for the second 
%                           one)
%   options.priors        = prior probability of the two classes, 1 by 2
%                           vector
%          .cov_type      = string containing the label for the class
%                           covariance type:
%                           ''             --> covariance
%                           'sph'          --> spherical covariance
%                           'nrm'          --> normalized covariance
%                           'diag'         --> diagonalized covariance
%          .K             = number of neighbours selected for the
%                           classification
%          .tieBreaker    = rule to breake tie event in classification:
%                           'random'       --> assign a random class
%                           'nearest'      --> assign the class of the
%                                              nearest training set element
%                           'farthest'     --> assign the oposite class of
%                                              the farthest training set
%                                              element
%          .distance      = type of distance:
%                           'sqeuclidean'  --> squared euclidean distance
%                           'cityblock'    --> taxicab geometry
%                           'cosine' or
%                           'correlation'  --> correlation distance
%                           'hammming'     --> hamming distance (only for
%                                              binary data)
% 
% Output:
%   knnStruct.type        = string containing the type of discriminant
%                           analysis (knn) 
%            .priors      = prior probability of the two classes, 1 by 2
%                           vector
%            .cov_type    = string containg the label for the class
%                           covariance type (same available values of 
%                           options.cov_type input)
%            .K           = number of neighbours selected for the
%                           classification
%            .tieBreaker  = rule to breake tie event in classification
%                           (same available values of options.tieBreaker
%                           input)
%            .distance    = type of distance(same available values of 
%                           options.distance input)
%            .trainSet    = train set data, N by d matrix (observations in
%                           rows, features in columns)
%            .trainLabel  = train class labels, N by 1 vector
%            .m1          = mean of the first class
%            .m2          = mean of the second class
%            .cov1        = covariance of the first class
%            .cov2        = covariance of the second class
% 
% See also knn_test, knn_example, classTrain, classTest


% Number of input arguments controllers
if nargin < 2
    error('Error. Not enogh input arguments');
end

if nargin > 3
    error('Error. Too many input arguments');
end

% Fields name of the options struct input parameter
fpriors = 'priors';
fcov = 'cov_type';
fK = 'K';
ftieBreak = 'tieBreaker';
fdistance = 'distance';

if nargin == 2
    % Default priors value
    vpriors = {[0.5 0.5]
             };
    vcov = '';
    vK = round(sqrt(size(data,1)));
    vtieBreak = 'random';
    vdistance = 'sqeuclidean';

else
    % Check if options input prameters contains suitable fields
    opt_fields = fieldnames(options);
    indp = strmatch(fpriors, opt_fields);
    indc = strmatch(fcov, opt_fields);
    indk = strmatch(fK, opt_fields);
    indt = strmatch(ftieBreak, opt_fields);
    indd = strmatch(fdistance, opt_fields);
    % Check priors field presence
    if ~isempty(indp)
        vpriors = options.priors;
    else
        vpriors = [0.5 0.5];
        disp(['knn_train WARNING: "options" input parameter does not ' ...
              'contain suitable "priors" field; field is set ' ...
              'up with the default value']);
    end
    % Check covariance type presence
    if ~isempty(indc)
        vcov = options.cov_type;
    else
        vcov = '';
        disp(['knn_train WARNING: "options" input parameter does not ' ...
              'contain suitable "cov_type" field; field is set ' ...
              'up with the default value']);
    end
    % Check priors field presence
    if ~isempty(indk)
        vK = options.K;
    else
        vK = round(sqrt(size(data,1)));
        disp(['knn_train WARNING: "options" input parameter does not ' ...
              'contain suitable "K" field; field is set ' ...
              'up with the default value']);
    end
    if ~isempty(indt)
        vtieBreak = options.tieBreaker;
    else
        vtieBreak = 'random';
        disp(['qda_train WARNING: "options" input parameter does not ' ...
              'contain suitable "tieBreaker" field; field is set ' ...
              'up with the default value']);
    end
    if ~isempty(indd)
        vdistance = options.distance;
    else
        vdistance = 'sqeuclidean';
        disp(['qda_train WARNING: "options" input parameter does not ' ...
              'contain suitable "distance" field; field is set ' ...
              'up with the default value']);
    end
end

knnStruct = struct('type','knn',fpriors, vpriors, fcov, vcov, ...
    fK, vK, ftieBreak, vtieBreak, fdistance, vdistance);

% Dataset class subdivision
uniq_labs = unique(labels);
data_class_1 = data(labels==uniq_labs(1),:);
data_class_2 = data(labels==uniq_labs(2),:);
% Integration of training set in the structed model
knnStruct.trainSet = data;
knnStruct.trainLabel = labels;
% Classes mean computation
knnStruct.m1 = mean(data_class_1)';
knnStruct.m2 = mean(data_class_2)';
% Classes covariance computation
knnStruct.cov1 = cov(data_class_1);
knnStruct.cov2 = cov(data_class_2);
% 

% Selection of the covariance type
switch knnStruct.cov_type
    case ''         
        % do nothing
    case 'nrm'
        knnStruct.cov1 = covnormalization(knnStruct.cov1, ...
            size(data_class_1, 1));
        knnStruct.cov2 = covnormalization(knnStruct.cov2, ...
            size(data_class_2, 1));
    case 'sph'
        knnStruct.cov1  = knnStruct.cov1.*eye(size(knnStruct.cov1));
        knnStruct.cov2  = knnStruct.cov1.*eye(size(knnStruct.cov2));
    case 'diag'
        knnStruct.cov1 = diag(diag(knnStruct.cov1));
        knnStruct.cov2 = diag(diag(knnStruct.cov2));
    otherwise
        error(['Error. Such covariance type is not ' ...
            'implemented: change cov_type field value']);
end
            

end
