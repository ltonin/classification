function [postprob, res] = knn_test(knnStruct,data)
% Use:
%   [postprob, class] = knn_test(knnStruct,data)
% 
% Input :
%   knnStruct.type        = string containing the type of discriminant
%                           analysis (knn) 
%            .priors      = prior probability of the two classes, 1 by 2
%                           vector
%            .cov_type    = string containing the label for the class
%                           covariance type:
%                           ''             --> covariance
%                           'sph'          --> spherical covariance
%                           'nrm'          --> normalized covariance
%                           'diag'         --> diagonalized covariance
%            .K           = number of neighbours selected for the
%                           classification
%            .tieBreaker  = rule to breake tie event in classification:
%                           'random'       --> assign a random class
%                           'nearest'      --> assign the class of the
%                                              nearest training set element
%                           'farthest'     --> assign the oposite class of
%                                              the farthest training set
%                                              element
%            .distance    = type of distance:
%                           'sqeuclidean'  --> squared euclidean distance
%                           'cityblock'    --> taxicab geometry
%                           'cosine' or
%                           'correlation'  --> correlation distance
%                           'hammming'     --> hamming distance (only for
%                                              binary data)
%            .trainSet    = train set data, N by d matrix (observations in
%                           rows, features in columns)
%            .trainLabel  = train class labels, N by 1 vector
%            .m1          = mean of the first class
%            .m2          = mean of the second class
%            .cov1        = covariance of the first class
%            .cov2        = covariance of the second class
%   data                  = predictor data, N by 2 matrix (observations in
%                           rows, features in columns)   
%
%  Output:
%   postprob              = postirior class belonging probabilities, N by 2
%                           matrix (observations in rows, features in
%                           columns)
%   class                 = prediction of class belonging, N by 1 vector
%                           (observation in rows, features in columns)
% 
% See also knn_train, knn_example, classTrain, classTest

% Parameters extraction from the input model
trainSet = knnStruct.trainSet;
trainLab = knnStruct.trainLabel;
priors = knnStruct.priors;
m1 = knnStruct.m1;
m2 = knnStruct.m2;
SIGMA1 = knnStruct.cov1;
SIGMA2 = knnStruct.cov2;
K = knnStruct.K;
tieBreaker = knnStruct.tieBreaker;
distance=knnStruct.distance;

% Auxiliary parameter
[numObs, d] = size(data);
numClass = length(priors);

% Output parameters initialization
postprob = zeros(numObs, 2);
res = zeros(numObs, 1);

% Distance calculation
testDist = distfun(data,trainSet,distance);
% Neighbours class definition
[dSorted, dIndex] = sort(testDist,2);
dIndex = dIndex(:,1:K);
neighbLab = trainLab(dIndex);
if size(neighbLab,2) ==1
    neighbLab = neighbLab';
end
% Occurences of the classes couting
counts = zeros(numObs,numClass);

for indObs = 1:numObs
    for indK = 1:K
        counts(indObs,neighbLab(indObs,indK)) = counts(indObs, ...
            neighbLab(indObs, indK)) + 1;
    end
end
% Class assignation to test set observation
[L,res] = max(counts,[],2);

% Check for possible ties: case where L = K/2
ties = find(L==(K/2));
numTies = length(ties);
% Tie rule selection
switch tieBreaker
    case 'random'
        for indTies = 1:numTies
            % random tie break
            res(ties(indTies)) = randsample(numClass,1);
        end
    case 'nearest'
        for indTies = 1:numTies
            % assign the class of the closest element to break the tie
            res(ties(indTies)) = neighbLab(1,ties(indTies));
        end
    case 'farthest'
        for indTies = 1:numTies
            % assign the opposite class of the farthest element to break
            % the tie
            res(ties(indTies)) = neighbLab(K,ties(indTies));
        end
end


for indObs = 1:numObs
    postprob(indObs,res(indObs)) = L(indObs)/K;
    postprob(indObs,mod(res(indObs),2)+1) = (K-L(indObs))/K;
end

end

function D = distfun(Train, Test, dist)
%DISTFUN Calculate distances from training points to test points.
[n,p] = size(Train);
D = zeros(n,size(Test,1));
numTest = size(Test,1);

switch dist
    case 'sqeuclidean'
        for i = 1:numTest
            D(:,i) = sum((Train - Test(repmat(i,n,1),:)).^2, 2);
        end
    case 'cityblock'
        for i = 1:numTest
            D(:,i) = sum(abs(Train - Test(repmat(i,n,1),:)), 2);
        end
    case {'cosine','correlation'}
        % Normalized both the training and test data.
        normTrain = sqrt(sum(Train.^2, 2));
        normTest = sqrt(sum(Test.^2, 2));
        normData = sqrt(sum([Train;Test].^2, 2));
        Train = Train ./ normTrain(:,ones(1,size(Train,2)));
        if any(normData < eps) % small relative to unit-length data points
            error('stats:knn:ZeroTestentroid', ...
                'Zero cluster centroid created at iteration %d.',iter);
        end
        % This can be done without a loop, but the loop saves memory
        % allocations
        for i = 1:numTest
            D(:,i) = 1 - (Train * Test(i,:)') ./ normTest(i);
        end
    case 'hamming'
        if ~all(ismember(Test(:),[0 1]))
            error(['Non-binary data cannot be clustered using Hamming ' ...
                'distance.']);
        end
        for i = 1:numTest
            D(:,i) = sum(abs(Train - Test(repmat(i,n,1),:)), 2) / p;
        end
    otherwise
        error(['Error. Such distance type is not ' ...
            'implemented: change distance field value']);
end

end