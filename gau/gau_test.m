function [postprob, class] = gau_test(gauStruct, data)
% Use:
%   [postprob, class] = gau_test(gauStruct, data)
% 
% Input :
%   gauStruct.type     = string containing the type of discriminant
%                        analysis (gau) 
%            .M        = means, consisting in the centers of the gaussian
%                        kernels, 2 by J by K matrix (classes in first     
%                        dimension, number of prototype in the second 
%                        dimension and space feature in the third one)
%            .C        = covariance, 2 by J by K matrix (classes in first     
%                        dimension, number of prototype in the second 
%                        dimension and space feature in the third one); the
%                        variable contains only variance values, supposed
%                        to be a diagonalize covariance matrix 
%            .map      = size of the 2D map used to create Self-Organizing
%                        Maps(SOM), 1 by 2 array.
%            .shared   = string containing a label for the covariance  
%                        sharing among the two classes:
%                        't'    --> true, covariance sharing
%                        'f'    --> false, no covariance sharing
%            .traino   = use 0 to disable, 1 for CLI, 2 for GUI 
%            .NumEp    = integer expressing thenumber of epoches used for
%                        training the model
%   data               = predictor data, N by 2 matrix (observations in
%                        rows, features in columns)   
%
%  Output:
%   postprob           = postirior class belonging probabilities, N by 2
%                        matrix (observations in rows, features in
%                        columns)
%   class              = prediction of class belonging, N by 1 vector
%                        (observation in rows, features in columns)
% 
% See also gau_train, gau_example, classTrain, classTest

    M = gauStruct.M;
    C = gauStruct.C;
    
    NumSamples = size(data, 1);
    postprob = zeros(NumSamples, 2);
    
    for s = 1:NumSamples
        [~, postprob(s, :)] = gauClassifier(M, C, data(s, :));
        [~, maxInd] = max(postprob(s,:));
        class(s) = maxInd;
    end
    
end

function [activities, probabilities] = ...
    gauClassifier(centers, covariances, sample)

%  This function classifies a sample, using a given gaussian model (centers 
%  and covariances). It returns a 2D array of activities (activity of each 
%  prototype of each class) and a 1D array of probabilities (probability of 
%  each class)
%
% Use:
%  [activities, probabilities] = gauClassifier(centers, covariances, sample)
%
% Inputs:
%  centers        = 3D array of size ixjxk, where i is the number of 
%                   classes, j is the number of prototypes per class and k
%                   is the space dimensionality
%  covariances    = 3D array of size ixjxk, where i is the number of,
%                    classes, j is the number of prototypes per class and
%                    k is the space dimensionality
%  sample         = 1D array of size k, where k is the space dimensionality
%
% Outputs:
%  activities     = 2D array of size ixj, where i is the number of classes
%                   and j is the number of prototypes per class
%  probabilities  = 1D array of size i, where i is the number of classes  

    model_dimensions = [size(centers,1), size(centers,2), size(centers,3)];                                                   % Dimensions of the model

    for i = 1:model_dimensions(1)
        for j = 1:model_dimensions(2)
            determinant = 1;                                 
            M = reshape(centers(i,j,:), [1 model_dimensions(3)]);
            C = reshape(covariances(i,j,:), [1 model_dimensions(3)]);
            distance = (sample - M).^2 ./ C;
            determinant_temp = determinant * sqrt(C);

            % Test to prevent determinant = 0
            determinant_temp(find(determinant_temp==0))=1;    
            determinant = prod(determinant_temp);
            % Total distance to sample for prototype j of class i
            distance_sum = sum(distance);         
            % Activity of prototype j of class i
            activities(i,j) = exp(-distance_sum/2) / determinant; 
        end
    end

    % Unnormalized probabilities
    raw_probabilities = sum(activities,2)';       
    % Sum of unnormalized probabilities
    probabilities_sum = sum(raw_probabilities);   

    if (probabilities_sum  ~= 0)
        % Normalized probabilities
        probabilities = raw_probabilities / probabilities_sum;   
    else
        probabilities = ones(1,model_dimensions(1)) /model_dimensions(1); 
    end
end

