% Example code to use gaussian classifier
clc;clear all;close all;

npat = 1000;

% create synthetic train data
r1 = 0.5 + 0.95*randn(npat,2);
r2 = 3.5 + 0.95*randn(npat*2,2);
data = [r1;r2];
labels = [ones(1,size(r1,1)) 2*ones(1,size(r2,1))];

% train gaussian classifier
options = struct('NumEp',2);
gauStruct = classTrain(data,labels','gau',options);

% test gau
% create synthetic test data
r1_test = 0.5 + 0.95*randn(npat/2,2);
r2_test = 3.5 + 0.95*randn(npat/2,2);
data_test = [r1_test;r2_test];
labels_test = [ones(1,size(r1_test,1)) 2*ones(1,size(r2_test,1))];

[pp res] = classTest(gauStruct,data_test);

accuracy  = 100*sum(res'==labels_test)/length(labels_test);

% graphics
figure(1)
plot(data(labels==1,1),data(labels==1,2),'.') ; hold on;
plot(data(labels==2,1),data(labels==2,2),'gx');
plot(data_test(labels_test==1,1),data_test(labels_test==1,2),'^') ;
plot(data_test(labels_test==2,1),data_test(labels_test==2,2),'gv');
mis_ind = find(res'~=labels_test);
plot(data_test(mis_ind,1),data_test(mis_ind,2),'ro','markersize',10);
axis([0 6 0 6]);
legend('class-1_{train}', 'class-2_{train}', 'class-1_{test}', ...
    'class-2_{test}', 'misclassified');
hold off


[tpr fpr auc acc mcc] = stat_roccurve(labels_test, pp(:,1)',0.01);
auc
figure(2)
plot(fpr,tpr); hold on;
plot([0 1], [0 1], 'r--');






