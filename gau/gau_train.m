function gauStruct = gau_train(data, labels, options)
% Use:
%   [gauStruct] = gau_train(data,labels,options)
% 
% Input:
%   data               = predictor data, N by d matrix (observations in
%                        rows, features in columns)
%   labels             = class labels, N by 1 vector (where value 1 stays
%                        for the first class and 2 for the second one)
%   options.map        = size of the 2D map used to create Self-Organizing
%                        Maps(SOM), 1 by 2 array. For example map = [3 2]
%                        if you want 3x2 = 6 prototypes per class
%          .shared     = string containing a label for the covariance  
%                        sharing among the two classes prototypes:
%                        't'    --> true, covariance sharing
%                        'f'    --> false, no covariance sharing
%          .traino     = use 0 to disable, 1 for CLI, 2 for GUI. This
%                        switch was added to avoid having a gui running
%                        when connected remotely
% 
% Output:
%   gauStruct.type     = string containing the type of discriminant
%                        analysis (gau) 
%            .M        = means, consisting in the centers of the gaussian
%                        kernels, 2 by J by K matrix (classes in first     
%                        dimension, number of prototype in the second 
%                        dimension and space feature in the third one)
%            .C        = covariance, 2 by J by K matrix (classes in first     
%                        dimension, number of prototype in the second 
%                        dimension and space feature in the third one); the
%                        variable contains only variance values, supposed
%                        to be a diagonalize covariance matrix 
%            .map      = size of the 2D map used to create Self-Organizing
%                        Maps(SOM), 1 by 2 array
%            .shared   = string containing a label for the covariance  
%                        sharing among the two classes (same available 
%                        values of options.shared input)
%            .traino   = use 0 to disable, 1 for CLI, 2 for GUI
%            .NumEp    = integer expressing the number of epoches used for
%                        training the model
% 
% See also gau_test, gau_example, classTrain, classTest


    % Number of input arguments controllers
    if nargin < 2
        error('Error. Not enogh input arguments');
    end

    if nargin > 3
        error('Error. Too many input arguments');
    end

    % Fields name of the options struct input parameter
    fmap = 'map';
    fshared = 'shared';
    ftraino = 'traino';
    fnumep = 'NumEp';

    if nargin == 2
        % Default map configuration
        vmap = [2 2];   % one layer with only one prototype
        % Defaut shared flag
        vshared = 't';  % different covariance between prototypes of 
                        % different class
        % Default traino flag
        vtraino = 0;
        % Default epochs number
        vnumep = 5;
    else
        % Check if options input prameters contains suitable fields
        opt_fields = fieldnames(options);
        indm = strmatch(fmap, opt_fields);
        inds = strmatch(fshared, opt_fields);
        indt = strmatch(ftraino, opt_fields);
        indn = strmatch(fnumep, opt_fields);
        % Check map field presence
        if ~isempty(indm)
            vmap = options.map;
        else
            vmap = [2 2];
            disp(['gau_train WARNING: "options" input parameter does ' ... 
                  'not contain suitable "map" field; field is set ' ...
                  'up with the default value']);
        end
        % Check shared field presence
        if ~isempty(inds)
            vshared = options.shared;
        else
            vshared = 't';
            disp(['gau_train WARNING: "options" input parameter does ' ...
                  'not contain suitable "t" field; field is set ' ...
                  'up with the default value']);
        end
        % Check traino field presence
        if ~isempty(indt)
            vtraino = options.traino;
        else
            vtraino = 0;
            disp(['gau_train WARNING: "options" input parameter does ' ...
                  'not contain suitable "traino" field; field is set ' ...
                  'up with the default value']);
        end
        % Check NumEp field presence
        if ~isempty(indn)
            vnumep = options.NumEp;
        else
            vnumep = 5;
            disp(['gau_train WARNING: "options" input parameter does ' ...
                  'not contain suitable "NumEp" field; field is set ' ...
                  'up with the default value']);
        end
    end

    gauStruct = struct('type','gau',fmap, vmap, fshared, vshared, ...
        ftraino, vtraino, fnumep, vnumep);

    % Auxiliary variables
    P = cat(2, data, labels);

    % Gaussian prototypes calculation
    [M{1}, C{1}] = gauInitialization(P, gauStruct.map, ...
        gauStruct.shared, gauStruct.traino);


    % Control parameters
    th = 0.7;
    mimean = 0.0001;
    micov  = 0.00001;

    disp('Training:')
    for ep = 1:gauStruct.NumEp
        disp(['         -computing epoch #' num2str(ep)]);
        [cm, pv] = gauEval(M{end}, C{end}, P, th);
        perf(ep) = 1.00 - pv(2);
        rej(ep)  = pv(3);
        conf{ep} = cm(end/2:end, :);

        cc(ep) = eegc3_channel_capacity(1 - perf(ep), rej(ep), 2);

        [M{end + 1}, C{end + 1}] = gauUpdate(M{end}, C{end}, P, ...
                                    mimean, micov, gauStruct.shared);
    end

    [~, epMax] = max(cc(2:end));
    epMax = epMax + 1;

    gauStruct.M = M{epMax};
    gauStruct.C = C{epMax};

end

function [centers, covariances] = ...
    gauInitialization (set, map, shared, traino)

% Initialises the values of the centers and covariances for further 
% learning, using a 2D array of samples, a 1D array with the size of the 
% 2D map to create, and a booloean to specify if all prototypes of all 
% class should use the same covariances.
%
% Use:
%   [gauStruct] = gau_train(data,labels,options)
% 
% Input:
%   set         = a 2D array of size nx(k+1) where n is the number of 
%                 samples and k is the space dimensionality. The last
%                 column of set must contain the class of the samples
%
%   map         = a 1D array specifying the size of the 2D map to create
%                 using Self-Organizing Maps (SOM). For example map = [3 2]
%                 if you want 3x2 = 6 prototypes per class
%
%   shared      = string containing a label for the covariance sharing
%                 among the two classes:
%                 't'    --> true, covariance sharing
%                 'f'    --> false, no covariance sharing
%   traino      = use 0 to disable, 1 for CLI, 2 for GUI. This switch was
%                 added to avoid having a gui running when connected
%                 remotely
% Output:
%   centers     = matrix containing the centers of the gaussian prototypes
%   covariances = matrix containing the covariance between the gaussian
%                 center prototipes an the observations 
% Initialises the centres and covariances of the gaussian prototypes
%

% Dimensions nx(k+1) of the set
set_dimensions = size(set);
% Classes of the samples
set_classes = set(:,set_dimensions(2));
% Samples
set_data = set(:,1:set_dimensions(2)-1);

% Sorted list of the unique classes of the samples
classes = unique(set_classes);
% Number of unique classes                                        
classes_number = length(classes);                                               

    for i = 1:classes_number

        % Extraction of samples of class i
        data = set_data(find(set_classes==classes(i)),:);                           
        % Dimensions of the sub-array of samples of class i
        data_dimensions = size(data);                                               

        net = newsom(data', map);                                            

        net.trainParam.epochs = 400;     % SOM will perform 400 iterations                                             
        net.trainParam.showCommandLine = false;
        net.trainParam.showWindow =  false;
        if(traino == 1)
            net.trainParam.showCommandLine = true;
        elseif (traino == 2)
            net.trainParam.showWindow =  true;
        end

        net = train(net, data');                                         
        % Extraction of the centers after SOM training
        centers(i,:,:) = net.iw{1};

        for k = 1:map(1)*map(2)
            for l = 1:data_dimensions(2)
                % Declaration of the covariances
                covariances_raw(i,k,l) = 0;                            
            end
        end
        for j = 1:data_dimensions(1)
            for k = 1:map(1)*map(2)
                for l = 1:data_dimensions(2)
                    % Distance of sample to centers of class i
                    difference(k,l) = (data(j,l) - centers(i,k,l))^2;               
                end
                % Total distance
                distance(k) = sum(difference(k,:));                                 
            end
            % Minimum distance
            [~, index] = min(distance);                                        
            for k = 1:map(1)*map(2)
                for l = 1:data_dimensions(2)
                    % Initialisation of covariances using the closest
                    % prototype
                    covariances_raw(i,k,l) = covariances_raw(i,k,l) ...
                        + difference(index,l)/data_dimensions(1);                   
                end
            end
        end    
        clear data;                                                                 
        clear net;                                                        
    end
    
    % Dimensions of the model
    model_dimensions  = [size(covariances_raw,1), ...
                         size(covariances_raw,2), ...
                         size(covariances_raw,3)];    

    for i = 1:model_dimensions(1)
        for j = 1:model_dimensions(2)
            for k = 1:model_dimensions(3)
                if shared == 't'
                    % Shared covariances, all prototypes of all classes 
                    % have the same covariances
                    covariances(i,j,k) = mean(covariances_raw(:,j,k));            
                else                                                               
                    % Unshared covariances, all prototypes of a given class
                    % have the same covariances, but prototypes of d
                    % differentclasses have different covariances
                    covariances(i,j,k) = covariances_raw(i,j,k);                    
                end                                                               
            end                                                                   
        end
    end
end

function [conf_matrix, perf] = gauEval(centers, covariances, ...  
    set, prob_thresh)
% Use:
%   [conf_matrix, perf] = gauEval(centers, covariances, set, prob_thresh)
%   
% Evaluates the performances of a given model (centers and covariances) 
% on a given set using a probability threshold.
%
% Inputs:
%   centers       = 3D array of size ixjxk, where i is the number of 
%                   classes, j is the number of prototypes per class and k
%                   is the space dimensionality
%   covariances   = 3D array of size ixjxk, where i is the number of 
%                   classes, j is the number of prototypes per class and k
%                   is the space dimensionality.
%   set           = a 2D array of size nx(k+1) where n is the number of
%                   samples and k is the space dimensionality. The last
%                   column of set must contain the class of the samples
%   prob_thresh   = scalar, the probability threshold
%
% Outputs:
%   conf_matrix   = 2D array, the confusion matrix, absolute and relative 
%                   values
%   perf          = 1D array of size 3. It contains the error function, the 
%                   percentage of miss-classfied samples and the percentage
%                   of non-classified samples.

    % Dimensions nx(k+1) of the set
    set_dimensions = size(set);
    % Labels of the samples
    set_classes = set(:,set_dimensions(2)); 
    % Samles
    set_data = set(:,1:set_dimensions(2)-1);   

    % Sorted list of the unique classes of the samples
    classes = unique(set_classes);
    % Number of unique classes
    classes_number = length(classes);                          

    % Initialisation of absolute confusion matrix
    conf_matrix_abs = zeros (classes_number, classes_number+1);                         

    % Classification of sample i
    for i = 1:set_dimensions(1)
        [~, probabilities] = ...
            gauClassifier(centers, covariances, set_data(i,:));
        % Highest probability, recognised class = classes(index_class)
        [max_prob, index_class] = max(probabilities);
        % Class index for label of current sample
        index_label=find(classes==set_classes(i));
        % Updating absolute confusion matrix
        if max_prob < prob_thresh
            % Unclassified if under the threshold
            conf_matrix_abs(index_label,classes_number+1) = ...  
                conf_matrix_abs(index_label,classes_number+1)+1;
        else
            % Updating absolute confusion matrix
            conf_matrix_abs(index_label,index_class) = ...
                conf_matrix_abs(index_label,index_class)+1;                             
        end
        % Initialisation of target
        target = zeros(1,classes_number);
        % Target = 1 for class of sample i
        target(find(classes==set_classes(i))) = 1;

        % Scaling factors
        scaling_factors = gauScaling(set);
        % Error function for sample i
        error(i) = sum(((probabilities - target).^2).*scaling_factors);
    end
    % Total error function
    err_function = sum(error)/set_dimensions(1)/2;

    for i = 1:classes_number
        % Relative confusion matrix
        conf_matrix_rel(i,:) = ...
            100*conf_matrix_abs(i,:)/sum(conf_matrix_abs(i,:));
    end
    
    % Correctly classified samples
    correct_class = sum(diag(conf_matrix_abs));

    % Non-classified samples
    non_class_abs = sum(conf_matrix_abs(:,classes_number+1));
    % Ratio of non-classified samples
    non_class = non_class_abs/set_dimensions(1);

    % Miss-classified samples
    miss_class_abs = set_dimensions(1) - correct_class - non_class_abs;
    % Ratio of miss-classified samples
    miss_class = miss_class_abs/set_dimensions(1);

    % General confusion matrix
    conf_matrix=[conf_matrix_abs;conf_matrix_rel];
    % Performancset_dimensionses
    perf = [err_function, miss_class, non_class];

end

function scaling_factors = gauScaling(set)  

% Use:
%   scaling_factors = gauScaling(set)
%
% Returns scaling factors to compensate the eventual 
% different numbers of samples of each class in the set
%
% Inputs:
%   set              = a 2D array of size nx(k+1) where n is the number of 
%                      samples and k is the space dimensionality. The last
%                      column of set must contain the class of the samples
%
% Outputs
%   scaling_factors  = 1D array of size i, where i is the number of classes

    % Dimensions nx(k+1) of the set
    set_dimensions = size(set);
    % Labels of the samples
    set_classes = set(:,set_dimensions(2)); 
    % Samles
    set_data = set(:,1:set_dimensions(2)-1);   

    % Sorted list of the unique classes of the samples
    classes = unique(set_classes);
    % Number of unique classes
    classes_number = length(classes);        
    for j = 1:classes_number
        % Number of samples of each class
        classes_weight(j) = size(find(set_classes==classes(j)),1);                  
    end
    
    % Weight of the classes
    classes_weight = 0.2 + 0.8./ ...
        (classes_weight*classes_number/set_dimensions(1));
    % Scaling_factors
    scaling_factors = classes_weight./max(classes_weight);

end

function [activities, probabilities] = ...
    gauClassifier(centers, covariances, sample)

%  This function classifies a sample, using a given gaussian model (centers 
%  and covariances). It returns a 2D array of activities (activity of each 
%  prototype of each class) and a 1D array of probabilities (probability of 
%  each class)
%
% Use:
%  [activities, probabilities] = gauClassifier(centers, covariances, sample)
%
% Inputs:
%  centers        = 3D array of size ixjxk, where i is the number of 
%                   classes, j is the number of prototypes per class and k
%                   is the space dimensionality
%  covariances    = 3D array of size ixjxk, where i is the number of,
%                    classes, j is the number of prototypes per class and
%                    k is the space dimensionality
%  sample         = 1D array of size k, where k is the space dimensionality
%
% Outputs:
%  activities     = 2D array of size ixj, where i is the number of classes
%                   and j is the number of prototypes per class
%  probabilities  = 1D array of size i, where i is the number of classes  

    model_dimensions = [size(centers,1), size(centers,2), size(centers,3)];                                                   % Dimensions of the model

    for i = 1:model_dimensions(1)
        for j = 1:model_dimensions(2)
            determinant = 1;                                 
            M = reshape(centers(i,j,:), [1 model_dimensions(3)]);
            C = reshape(covariances(i,j,:), [1 model_dimensions(3)]);
            distance = (sample - M).^2 ./ C;
            determinant_temp = determinant * sqrt(C);

            % Test to prevent determinant = 0
            determinant_temp(find(determinant_temp==0))=1;    
            determinant = prod(determinant_temp);
            % Total distance to sample for prototype j of class i
            distance_sum = sum(distance);         
            % Activity of prototype j of class i
            activities(i,j) = exp(-distance_sum/2) / determinant; 
        end
    end

    % Unnormalized probabilities
    raw_probabilities = sum(activities,2)';       
    % Sum of unnormalized probabilities
    probabilities_sum = sum(raw_probabilities);   

    if (probabilities_sum  ~= 0)
        % Normalized probabilities
        probabilities = raw_probabilities / probabilities_sum;   
    else
        probabilities = ones(1,model_dimensions(1)) /model_dimensions(1); 
    end
end

function I = eegc3_channel_capacity(Pe, Pr, N)

    % Avoid to get NaN
    if(Pe == 0)
        Pe = 0.00000001;
    end
    if(Pe == 1)
        Pe = 0.99999999;
    end
    I = (1 - Pr)*(log2(N) + (1 - Pe)*log2(1-Pe) + Pe*log2(Pe/(N-1)));
end

function [centers, covariances] = gauUpdate(centers, covariances, set, ...             
    centers_rate, covariances_rate, shared)            

%  This function updates the centers and covariances of the model for each 
%  sample of the set using the given learning rates for centers and 
%  covariances and a booloean to specify if all prototypes of all class 
%  should use the same covariances
% Use:
%  function [centers, covariances] = gauUpdate(centers, covariances, set, centers_rate, covariances_rate, shared)
% 
% Inputs:
%   centers           = 3D array of size ixjxk, where i is the number of
%                       classes, j is the number of prototypes per class
%                       and k is the space dimensionality
%   covariances       = 3D array of size ixjxk, where i is the number of
%                       classes, j is the number of prototypes per class
%                       and k is the space dimensionality.
%   set               = a 2D array of size nx(k+1) where n is the number of
%                       samples and k is the space dimensionality. The last
%                       column of set must contain the class of the samples
%   centers_rate      = a scalar, the learning rate for the centers
%   covariances_rate  = a scalar, the learning rate for the covariances
%   shared            = a boolean ('f' or 't') for unshared/shared
%                       covariances
%
% Outputs
%   centers           = 3D array of size ixjxk, where i is the number of
%                       classes, j is the number of prototypes per class
%                       and k is the space dimensionality
%
%   covariances       = 3D array of size ixjxk, where i is the number of
%                       classes, j is the number of prototypes per class
%                       and k is the space dimensionality.
    
    model_dimensions = [size(centers,1), size(centers,2), size(centers,3)]; 
    % Dimensions nx(k+1) of the set
    set_dimensions = size(set);
    % Labels of the samples
    set_classes = set(:,set_dimensions(2)); 
    % Samles
    set_data = set(:,1:set_dimensions(2)-1);   

    % Sorted list of the unique classes of the samples
    classes = unique(set_classes);
    % Number of unique classes
    classes_number = length(classes);                          
    
    % Scaling factors
    scaling_factors = gauScaling(set);

    for i = 1:set_dimensions(1)
        % Activities and probabilities for sample i
        [activities, probabilities] = ...
            gauClassifier(centers, covariances, set_data(i,:));
        sum_activities = sum(sum(activities));

        if (sum_activities ~=0)
            % Normalized activities
            activities = activities/sum_activities;
        end

        % Target initialisation
        target = zeros(1,classes_number);
        % Target = 1 if class match the class of the sample
        target(find(classes == set_classes(i))) = 1;

        % Pseudo error function
        error = sum((target-probabilities).*probabilities);
        % Activity factors
        factor = (target - probabilities - error).*scaling_factors;                     

        % Weighted activities
        activities = activities .* repmat(factor',[1,model_dimensions(2)]);

        % Rates for the centers
        laRate = centers_rate*activities;
        % Rates for the covariances
        lbRate = covariances_rate*activities;

        for j = 1:model_dimensions(1)
            for k = 1:model_dimensions(2)
                for l = 1:model_dimensions(3)
                    % Centers update
                    centers_new(j,k,l) = centers(j,k,l) + laRate(j,k)*...
                        (set_data(i,l) - centers(j,k,l))/ ...
                        covariances(j,k,l);
                    % Covariances update
                    covariances_new(j,k,l) = covariances(j,k,l)*...
                        exp(lbRate(j,k)*(((set_data(i,l) - ...
                        centers(j,k,l))^2)/(covariances(j,k,l)^1.5)));
                end
            end
        end

        centers = centers_new;

        % Averaging covariances of same class
        covariances_unshared = repmat(mean(covariances_new,2), ...
            [1,model_dimensions(2)]);

        if shared =='t'
            % Shared covariances if shared = 't'
            covariances = repmat(mean(covariances_unshared,1), ...
                [model_dimensions(1),1]);
        else
            % Unshared covariances if shared = 'f'
            covariances = covariances_unshared;                                         
        end
    end
    
end
    
    % Pierre Ferrez, IDIAP, pierre.ferrez@idiap.ch, March 2005
    % Modified by Cristina de Negueruela, IDIAP, January 2007
    % Modified by Andrea Cimolato, UNIPD, November 2015